package uptake.model;

/**
 * Created by Andrew on 12/11/2015.
 */
public class Corner {

    /*Corner of a room represented as an x,y coordinate*/

    private double x;
    private double y;

    public Corner() {
    }

    public Corner(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Corner corner = (Corner) o;

        if (Double.compare(corner.x, x) != 0) return false;
        return Double.compare(corner.y, y) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
