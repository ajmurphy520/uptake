package uptake.model;

import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Andrew on 12/11/2015.
 */
public class Room {

    /*Room of a house represented as a unique set of x, y coordinates*/

    private Long id;
    @Field(type = FieldType.Nested, includeInParent = true)
    private LinkedHashSet<Corner> corners;
    private String name;

    @Transient
    private double area;

    public Room() {
        this.id = 0L;
    }

    public Room(Long id, LinkedHashSet<Corner> corners, String name) {
        this.id = id;
        if (corners != null) {
            this.corners = new LinkedHashSet<Corner>(corners);
        }
        this.name = name;
        findArea();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Corner> getCorners() {
        return corners;
    }

    public void setCorners(LinkedHashSet<Corner> corners) {
        if (corners != null) {
            this.corners = new LinkedHashSet<Corner>(corners);
        } else {
            this.corners = new LinkedHashSet<Corner>();
        }
        findArea();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getArea() {
        return area;
    }

    /*Finds area of room by calcuating area between each line segment and the y axis. Due to ordering of points this
    * function ends up adding the area from the far side segments and subtracting area of close side segments. This
    * cancels out the area between the y axis and the room that isn't in the room.*/
    private void findArea() {
        double sum = 0;
        if (corners.size() >= 3) {
            Iterator<Corner> cornerIterator = corners.iterator();
            Corner first = cornerIterator.next();
            Corner current;
            Corner previous = first;
            while (cornerIterator.hasNext()) {
                current = cornerIterator.next();
                sum += (previous.getX() * current.getY() - previous.getY() * current.getX());
                previous = current;
            }
            sum += (previous.getX() * first.getY() - previous.getY() * first.getX());
            area = (double) Math.round(Math.abs(sum / 2) * 100000d)/100000d; //Simple double rounding. Accurate enough for area
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Room room = (Room) o;

        if (!id.equals(room.id)) return false;
        return corners.equals(room.corners);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + corners.hashCode();
        return result;
    }
}
