package uptake.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrew on 12/9/2015.
 */
@Document(indexName = "houses", type = "house", indexStoreType = "memory")
public class House {

    @Id
    private Long id;
    private String address;
    @Field(type = FieldType.Nested, includeInParent = true)
    private Map<Integer, Floor> floors = new HashMap<Integer, Floor>();

    public House() {
    }

    public House(Long id, String address, Map<Integer, Floor> floors) {
        this.id = id;
        this.address = address;
        this.floors = floors;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Map<Integer, Floor> getFloors() {
        return floors;
    }

    public void setFloors(Map<Integer, Floor> floors) {
        this.floors = floors;
    }

    public void addFloor(Floor floor) {
        floors.put(floor.getFloorNumber(), floor);
    }

    public double getArea() {
        double areaSum = 0;
        for (Floor floor : floors.values()) {
            areaSum += floor.getArea();
        }
        return (double) Math.round(areaSum * 100000d) / 100000d; //Simple double rounding. Accurate enough for area
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        House house = (House) o;

        if (address != null ? !address.equals(house.address) : house.address != null) return false;
        return !(floors != null ? !floors.equals(house.floors) : house.floors != null);

    }

    @Override
    public int hashCode() {
        int result = address != null ? address.hashCode() : 0;
        result = 31 * result + (floors != null ? floors.hashCode() : 0);
        return result;
    }
}
