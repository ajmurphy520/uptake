package uptake.model;

import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrew on 12/11/2015.
 */
public class Floor {

    private int floorNumber;
    @Field(type = FieldType.Nested, includeInParent = true)
    private Map<Long, Room> rooms = new HashMap<Long, Room>();

    public Floor() {
    }

    public Floor(int floorNumber, Map<Long, Room> rooms) {
        this.floorNumber = floorNumber;
        this.rooms = rooms;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    public void setFloorNumber(int floorNumber) {
        this.floorNumber = floorNumber;
    }

    public Map<Long, Room> getRooms() {
        return rooms;
    }

    public void setRooms(Map<Long, Room> rooms) {
        this.rooms = rooms;
    }

    public double getArea() {
        double areaSum = 0;
        for (Room room : rooms.values()) {
            areaSum += room.getArea();
        }
        return (double) Math.round(areaSum * 100000d) / 100000d; //Simple double rounding. Accurate enough for area
    }

    public void addRoom(Room room) {
        rooms.put(room.getId(), room);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Floor floor = (Floor) o;

        if (floorNumber != floor.floorNumber) return false;
        return !(rooms != null ? !rooms.equals(floor.rooms) : floor.rooms != null);

    }

    @Override
    public int hashCode() {
        int result = floorNumber;
        result = 31 * result + (rooms != null ? rooms.hashCode() : 0);
        return result;
    }
}
