package uptake.exception;

/**
 * Created by Andrew on 12/11/2015.
 */
public class FloorNotValidException extends Exception {

    public FloorNotValidException(String message) {
        super(message);
    }

    public FloorNotValidException(Throwable cause) {
        super(cause);
    }

    public FloorNotValidException(String message, Throwable cause) {
        super(message, cause);
    }
}
