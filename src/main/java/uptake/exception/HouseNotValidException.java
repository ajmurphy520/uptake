package uptake.exception;

/**
 * Created by Andrew on 12/11/2015.
 */
public class HouseNotValidException extends Exception {

    public HouseNotValidException(String message) {
        super(message);
    }

    public HouseNotValidException(Throwable cause) {
        super(cause);
    }

    public HouseNotValidException(String message, Throwable cause) {
        super(message, cause);
    }
}
