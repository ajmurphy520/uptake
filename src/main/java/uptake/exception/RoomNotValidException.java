package uptake.exception;

/**
 * Created by Andrew on 12/11/2015.
 */
public class RoomNotValidException extends Exception {

    public RoomNotValidException(String message) {
        super(message);
    }

    public RoomNotValidException(Throwable cause) {
        super(cause);
    }

    public RoomNotValidException(String message, Throwable cause) {
        super(message, cause);
    }
}
