package uptake;

import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import uptake.service.HouseService;
import uptake.service.HouseServiceImpl;

@SpringBootApplication
public class Application {

    @Bean
    HouseService houseService() {
        return new HouseServiceImpl();
    }

    @Bean
    Client elasticClient() {
        Settings settings = ImmutableSettings.builder().put("path.home", "elasticdata").build();
        Node node = NodeBuilder.nodeBuilder().clusterName("murphy-cluster").local(true).settings(settings).node();
        return node.client();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}