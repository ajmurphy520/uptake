package uptake.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uptake.exception.*;
import uptake.model.Floor;
import uptake.model.House;
import uptake.model.Room;
import uptake.repository.HouseRepository;
import uptake.service.geometry.HouseValidator;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Andrew on 12/9/2015.
 */

@Service("houseService")
@Transactional
public class HouseServiceImpl implements HouseService {

    @Autowired
    private HouseRepository repository;

    private static final Logger log = LoggerFactory.getLogger(HouseServiceImpl.class);

    private static final AtomicLong idCounter = new AtomicLong();

    private HouseValidator validator = new HouseValidator();

    @Override
    public long createHouse(House house) throws HouseNotValidException {
        if (repository.findOneByAddress(house.getAddress()) != null) {
            throw new HouseNotValidException("House already exists with that address.");
        }
        house.setId(idCounter.incrementAndGet());
        for (Floor floor : house.getFloors().values()) {
            for (Room room : floor.getRooms().values()) {
                if (room.getId() == 0) {
                    room.setId(idCounter.incrementAndGet());
                }
            }
        }
        validator.validateHouse(house);
        repository.save(house);
        return house.getId();
    }

    @Override
    public House getHouse(long houseId) {
        return repository.findOne(houseId);
    }

    @Override
    public void updateHouse(House house) throws HouseNotFoundException, HouseNotValidException {
        if (!repository.exists(house.getId())) {
            throw new HouseNotFoundException();
        }
        for (Floor floor : house.getFloors().values()) {
            for (Room room : floor.getRooms().values()) {
                if (room.getId() == 0) {
                    room.setId(idCounter.incrementAndGet());
                }
            }
        }
        validator.validateHouse(house);
        repository.save(house);
    }

    @Override
    public void deleteHouse(long houseId) throws HouseNotFoundException {
        if (!repository.exists(houseId)) {
            throw new HouseNotFoundException();
        }
        repository.delete(houseId);
    }

    @Override
    public boolean existsHouse(long houseId) {
        return repository.exists(houseId);
    }

    @Override
    public double getHouseArea(long houseId) throws HouseNotFoundException {
        House house = repository.findOne(houseId);
        if (house == null) {
            throw new HouseNotFoundException();
        }
        return house.getArea();
    }

    @Override
    public long createFloor(long houseId, Floor floor) throws FloorNotValidException, HouseNotFoundException {
        House house = repository.findOne(houseId);
        if (house == null) {
            throw new HouseNotFoundException();
        }
        if (house.getFloors().containsKey(floor.getFloorNumber())) {
            throw new FloorNotValidException(String.format("Floor number %s already exists in house %s.", floor.getFloorNumber(), houseId));
        }
        for (Room room : floor.getRooms().values()) {
            if (room.getId() == 0) {
                room.setId(idCounter.incrementAndGet());
            }
        }
        validator.validateFloor(floor);
        house.getFloors().put(floor.getFloorNumber(), floor);
        repository.save(house);
        return floor.getFloorNumber();
    }

    @Override
    public Floor getFloor(long houseId, int floorNumber) throws HouseNotFoundException {
        House house = repository.findOne(houseId);
        if (house == null) {
            throw new HouseNotFoundException();
        }
        return house.getFloors().get(floorNumber);
    }

    @Override
    public void updateFloor(long houseId, Floor floor) throws HouseNotFoundException, FloorNotFoundException, FloorNotValidException {
        House house = repository.findOne(houseId);
        if (house == null) {
            throw new HouseNotFoundException();
        }
        if (!house.getFloors().containsKey(floor.getFloorNumber())) {
            throw new FloorNotFoundException();
        }
        for (Room room : floor.getRooms().values()) {
            if (room.getId() == 0) {
                room.setId(idCounter.incrementAndGet());
            }
        }
        validator.validateFloor(floor);
        house.getFloors().put(floor.getFloorNumber(), floor);
        repository.save(house);
    }

    @Override
    public void deleteFloor(long houseId, int floorNumber) throws HouseNotFoundException, FloorNotFoundException {
        House house = repository.findOne(houseId);
        if (house == null) {
            throw new HouseNotFoundException();
        }
        if (!house.getFloors().containsKey(floorNumber)) {
            throw new FloorNotFoundException();
        }
        house.getFloors().remove(floorNumber);
        repository.save(house);
    }

    @Override
    public boolean existsFloor(long houseId, int floorNumber) throws HouseNotFoundException {
        House house = repository.findOne(houseId);
        if (house == null) {
            throw new HouseNotFoundException();
        }
        return house.getFloors().containsKey(floorNumber);
    }

    @Override
    public double getFloorArea(long houseId, int floorNumber) throws HouseNotFoundException, FloorNotFoundException {
        House house = repository.findOne(houseId);
        if (house == null) {
            throw new HouseNotFoundException();
        }
        if (!house.getFloors().containsKey(floorNumber)) {
            throw new FloorNotFoundException();
        }
        return house.getFloors().get(floorNumber).getArea();
    }

    @Override
    public long createRoom(long houseId, int floorNumber, Room room) throws HouseNotFoundException, FloorNotFoundException, RoomNotValidException {
        House house = repository.findOne(houseId);
        if (house == null) {
            throw new HouseNotFoundException();
        }
        Floor floor = house.getFloors().get(floorNumber);
        if (floor == null) {
            throw new FloorNotFoundException();
        }
        room.setId(idCounter.incrementAndGet());
        if (floor.getRooms().containsKey(room.getId())) {
            throw new RoomNotValidException(String.format("Room %s already exists on floor %s.", room.getId(), floorNumber));
        }
        floor.addRoom(room);
        validator.validateRoom(room);
        repository.save(house);
        return room.getId();
    }

    @Override
    public Room getRoom(long houseId, int floorNumber, long roomId) throws HouseNotFoundException, FloorNotFoundException {
        House house = repository.findOne(houseId);
        if (house == null) {
            throw new HouseNotFoundException();
        }
        Floor floor = house.getFloors().get(floorNumber);
        if (floor == null) {
            throw new FloorNotFoundException();
        }
        return floor.getRooms().get(roomId);
    }

    @Override
    public void updateRoom(long houseId, int floorNumber, Room room) throws HouseNotFoundException, FloorNotFoundException, RoomNotFoundException, RoomNotValidException {
        House house = repository.findOne(houseId);
        if (house == null) {
            throw new HouseNotFoundException();
        }
        Floor floor = house.getFloors().get(floorNumber);
        if (floor == null) {
            throw new FloorNotFoundException();
        }
        Room oldRoom = floor.getRooms().remove(room.getId());
        if (oldRoom == null) {
            throw new RoomNotFoundException();
        }
        validator.validateRoom(room);
        floor.addRoom(room);
        repository.save(house);
    }

    @Override
    public void deleteRoom(long houseId, int floorNumber, long roomId) throws HouseNotFoundException, FloorNotFoundException, RoomNotFoundException {
        House house = repository.findOne(houseId);
        if (house == null) {
            throw new HouseNotFoundException();
        }
        Floor floor = house.getFloors().get(floorNumber);
        if (floor == null) {
            throw new FloorNotFoundException();
        }
        Room oldRoom = floor.getRooms().remove(roomId);
        if (oldRoom == null) {
            throw new RoomNotFoundException();
        }
        repository.save(house);
    }

    @Override
    public boolean existsRoom(long houseId, int floorNumber, long roomId) throws HouseNotFoundException, FloorNotFoundException {
        House house = repository.findOne(houseId);
        if (house == null) {
            throw new HouseNotFoundException();
        }
        Floor floor = house.getFloors().get(floorNumber);
        if (floor == null) {
            throw new FloorNotFoundException();
        }
        return floor.getRooms().containsKey(roomId);
    }

    @Override
    public double getRoomArea(long houseId, int floorNumber, long roomId) throws HouseNotFoundException, FloorNotFoundException, RoomNotFoundException {
        House house = repository.findOne(houseId);
        if (house == null) {
            throw new HouseNotFoundException();
        }
        Floor floor = house.getFloors().get(floorNumber);
        if (floor == null) {
            throw new FloorNotFoundException();
        }
        Room room = floor.getRooms().get(roomId);
        if (room == null) {
            throw new RoomNotFoundException();
        }
        return room.getArea();
    }

    @Override
    public void cleanDatabase() {
        repository.deleteAll();
        idCounter.set(0);
    }
}
