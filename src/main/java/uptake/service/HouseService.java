package uptake.service;

import uptake.exception.*;
import uptake.model.Floor;
import uptake.model.House;
import uptake.model.Room;

/**
 * Created by Andrew on 12/10/2015.
 */
public interface HouseService {

    long createHouse(House house) throws HouseNotValidException;

    House getHouse(long houseId);

    void updateHouse(House house) throws HouseNotFoundException, HouseNotValidException;

    void deleteHouse(long houseId) throws HouseNotFoundException;

    boolean existsHouse(long houseId);

    double getHouseArea(long houseId) throws HouseNotFoundException;

    long createFloor(long houseId, Floor floor) throws HouseNotFoundException, FloorNotValidException;

    Floor getFloor(long houseId, int floorNumber) throws HouseNotFoundException;

    void updateFloor(long houseId, Floor floor) throws HouseNotFoundException, FloorNotFoundException, FloorNotValidException;

    void deleteFloor(long houseId, int floorNumber) throws HouseNotFoundException, FloorNotFoundException;

    boolean existsFloor(long houseId, int floorNumber) throws HouseNotFoundException;

    double getFloorArea(long houseId, int floorNumber) throws HouseNotFoundException, FloorNotFoundException;

    long createRoom(long houseId, int floorNumber, Room room) throws HouseNotFoundException, FloorNotFoundException, RoomNotValidException;

    Room getRoom(long houseId, int floorNumber, long roomId) throws HouseNotFoundException, FloorNotFoundException;

    void updateRoom(long houseId, int floorNumber, Room room) throws HouseNotFoundException, FloorNotFoundException, RoomNotFoundException, RoomNotValidException;

    void deleteRoom(long houseId, int floorNumber, long roomId) throws HouseNotFoundException, FloorNotFoundException, RoomNotFoundException;

    boolean existsRoom(long houseId, int floorNumber, long roomId) throws HouseNotFoundException, FloorNotFoundException;

    double getRoomArea(long houseId, int floorNumber, long roomId) throws HouseNotFoundException, FloorNotFoundException, RoomNotFoundException;

    void cleanDatabase();
}
