package uptake.service.geometry;

import uptake.model.Corner;

/**
 * Created by Andrew on 12/12/2015.
 */
public class LineSegment {

    protected Point point1;
    protected Point point2;
    protected long roomId;

    protected Point sweepingLineIntersection;

    public LineSegment(Point point1, Point point2) {
        setPoints(point1, point2);
    }

    public LineSegment(Corner corner1, Corner corner2, long roomId) {
        Point point1 = new Point(corner1);
        Point point2 = new Point(corner2);
        this.roomId = roomId;
        setPoints(point1, point2);
    }

    public LineSegment(double x1, double y1, double x2, double y2) {
        Point point1 = new Point(x1, y1);
        Point point2 = new Point(x2, y2);
        setPoints(point1, point2);
    }


    public void setPoints(Point point1, Point point2) {
        if (point1.equals(point2)) {
            throw new RuntimeException("Cannot create zero length line segment");
        }
        if (point1.compareTo(point2) < 0) {
            this.point1 = point1;
            this.point2 = point2;
        } else {
            this.point1 = point2;
            this.point2 = point1;
        }
        sweepingLineIntersection = point1;
    }

    public Point getPoint1() {
        return point1;
    }

    public Point getPoint2() {
        return point2;
    }

    /*Calculates cross product of the two vectors formed by point1 to point2 and point1 the parameter point. If the cross
    * product is positive that means the vector with the parameter point lies to the left. If it's negative it lies
    * to the right. If it's zero they are colinear.*/
    public double locationOfPointRelativeToSegment(Point point) {
        return HouseValidator.fuzzyMatch((point2.x - point1.x) * (point.y - point1.y) - (point.x - point1.x) * (point2.y - point1.y));
    }

    public static Point intersectExcludeEndPoints(LineSegment segment1, LineSegment segment2) {
        double endpoint1RelativeSegment2 = segment2.locationOfPointRelativeToSegment(segment1.point1);
        double endpoint2RelativeSegment2 = segment2.locationOfPointRelativeToSegment(segment1.point2);

        double endpoint1RelativeSegment1 = segment1.locationOfPointRelativeToSegment(segment2.point1);
        double endpoint2RelativeSegment1 = segment1.locationOfPointRelativeToSegment(segment2.point2);

        if (HouseValidator.fuzzyMatch(endpoint1RelativeSegment1 * endpoint2RelativeSegment1) < 0.0 && HouseValidator.fuzzyMatch(endpoint1RelativeSegment2 * endpoint2RelativeSegment2) < 0.0) {
            double segment1XDifference = segment1.point2.x - segment1.point1.x;
            double segment1YDifference = segment1.point2.y - segment1.point1.y;
            double segment2XDifference = segment2.point2.x - segment2.point1.x;
            double segment2YDifference = segment2.point2.y - segment2.point1.y;

            double parametricT = (segment2XDifference * (segment1.point1.y - segment2.point1.y) - segment2YDifference * (segment1.point1.x - segment2.point1.x)) /
                    (-1 * segment2XDifference * segment1YDifference + segment1XDifference * segment2YDifference);

            return new Point(segment1.point1.x + parametricT * segment1XDifference, segment1.point1.y + parametricT * segment1YDifference);
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LineSegment that = (LineSegment) o;

        if (roomId != that.roomId) return false;
        if (!point1.equals(that.point1)) return false;
        return point2.equals(that.point2);

    }

    @Override
    public int hashCode() {
        int result = point1.hashCode();
        result = 31 * result + point2.hashCode();
        result = 31 * result + (int) (roomId ^ (roomId >>> 32));
        return result;
    }
}
