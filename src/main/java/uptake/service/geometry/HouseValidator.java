package uptake.service.geometry;

import uptake.exception.FloorNotValidException;
import uptake.exception.HouseNotValidException;
import uptake.exception.RoomNotValidException;
import uptake.model.Corner;
import uptake.model.Floor;
import uptake.model.House;
import uptake.model.Room;

import java.util.*;

/**
 * Created by Andrew on 12/12/2015.
 */
public class HouseValidator {
    //If two floating point values are within TOLERANCE of each other they are considered equal. Addresses very close
    //rooms and floating point inprecision
    public static final double TOLERANCE = 1.0e-10;

    /*Used during comparison of doubles. Allows for matchings that are close or that should be the same if it weren't
     * for floating point imprecision. */
    public static double fuzzyMatch(double delta) {
        if ((delta > (-1) * HouseValidator.TOLERANCE) && (delta < HouseValidator.TOLERANCE)) {
            return 0.0;
        }
        return delta;
    }

    /*Checks if a room is valid. A valid room does not intersect itself and contains at least 3 corners.
    * Potential added validations for overlapping rooms on the same floor*/
    public void validateRoom(Room room) throws RoomNotValidException {
        String message = null;
        if (room.getCorners().size() < 3) {
            message = String.format("Room %s has fewer than 3 corners.", room.getId());
        } else if (!isSimplePolygon(room)) {
            message = String.format("Room %s is self intersecting.", room.getId());
        }
        if (message != null) {
            throw new RoomNotValidException(message);
        }
    }

    /*Validates a floor by validating each room on the floor*/
    public void validateFloor(Floor floor) throws FloorNotValidException {
        try {
            for (Room roomOnFloor : floor.getRooms().values()) {
                validateRoom(roomOnFloor);
            }
        } catch (RoomNotValidException e) {
            throw new FloorNotValidException(e);
        }
    }

    public void validateHouse(House house) throws HouseNotValidException {
        try {
            for (Floor floorInHouse : house.getFloors().values()) {
                validateFloor(floorInHouse);
            }
        } catch (FloorNotValidException e) {
            throw new HouseNotValidException(e);
        }
    }

    /*Checks if a room is a simple polygon. A simple polygon is a closed polygon that does not intersect itself.
    * The function checks if the room is self intersecting by creating a list of all intersections. If the list contains
    * more intersections than the room has corners or any intersection involves more than two walls then the room is
    * not a simple polygon.*/
    private boolean isSimplePolygon(Room room) {
        Map<Point, IntersectionPoint> results = findIntersectionsBruteForce(room);
        if (results.size() != room.getCorners().size()) {
            return false;
        }
        for (IntersectionPoint intersection : results.values()) {
            if (intersection.getSegmentsIntersecting().size() != 2) {
                return false;
            }
        }
        return true;
    }

    /*Brute force method of finding all intersection points from a set of line segments, in this case the walls of a room.*/
    private Map<Point, IntersectionPoint> findIntersectionsBruteForce(Room room) {
        Map<Point, IntersectionPoint> results = new HashMap<Point, IntersectionPoint>();
        LinkedList<LineSegment> segments = new LinkedList<LineSegment>();
        segments.addAll(convertRoomToSegments(room));
        while (!segments.isEmpty()) {
            LineSegment currentSegment = segments.poll();
            for (LineSegment innerTestSegment : segments) {
                /*Check if the first endpoint of the current segment is on the innerTest line between its two endpoints*/
                if (innerTestSegment.locationOfPointRelativeToSegment(currentSegment.point1) == 0.0 &&
                        currentSegment.point1.compareTo(innerTestSegment.point1) >= 0 &&
                        currentSegment.point1.compareTo(innerTestSegment.point2) <= 0) {

                    /*Find where the intersection is. First check the end points of the innerTestSegment then
                    * calculate the intersection if the intersection is not at the end points*/
                    if (currentSegment.point1.equals(innerTestSegment.point1)) {
                        addIntersectionPoint(results, currentSegment.point1, currentSegment, innerTestSegment);
                    } else if (currentSegment.point1.equals(innerTestSegment.point2)) {
                        addIntersectionPoint(results, currentSegment.point1, currentSegment, innerTestSegment);
                    } else {
                        addIntersectionPoint(results, currentSegment.point1, currentSegment, innerTestSegment);
                    }

                }

                /*Check if the second endpoint of the current segment is on the innerTest line between its two endpoints*/
                if (innerTestSegment.locationOfPointRelativeToSegment(currentSegment.point2) == 0.0 &&
                        currentSegment.point2.compareTo(innerTestSegment.point1) >= 0 &&
                        currentSegment.point2.compareTo(innerTestSegment.point2) <= 0) {

                    /*Find where the intersection is. First check the end points of the innerTestSegment then
                    * calculate the intersection if the intersection is not at the end points*/
                    if (currentSegment.point2.equals(innerTestSegment.point1)) {
                        addIntersectionPoint(results, currentSegment.point2, currentSegment, innerTestSegment);
                    } else if (currentSegment.point2.equals(innerTestSegment.point2)) {
                        addIntersectionPoint(results, currentSegment.point2, currentSegment, innerTestSegment);
                    } else {
                        addIntersectionPoint(results, currentSegment.point2, currentSegment, innerTestSegment);
                    }

                }

                /*Check if there is an intersection that doesn't involve any endpoints.*/
                Point nonEndpointIntersection = LineSegment.intersectExcludeEndPoints(currentSegment, innerTestSegment);
                if (nonEndpointIntersection != null) {
                    addIntersectionPoint(results, nonEndpointIntersection, currentSegment, innerTestSegment);
                }
            }
        }
        return results;
    }

    /*Turns a room with a set of corners into a set of linesegments connecting consecutive corners. Used for calcuating
    * intersections.*/
    private Set<LineSegment> convertRoomToSegments(Room room) {
        Set<LineSegment> segments = new HashSet<LineSegment>();
        Iterator<Corner> iterator = room.getCorners().iterator();
        Corner first = iterator.next();
        Corner current;
        Corner previous = first;
        while (iterator.hasNext()) {
            current = iterator.next();
            segments.add(new LineSegment(previous, current, room.getId()));
            previous = current;
        }
        segments.add(new LineSegment(previous, first, room.getId()));
        return segments;
    }

    /*Logs the intersection of two line segments in the intersection results. If intersection point already exists it
    * adds the segments to the existing intersection point*/
    private void addIntersectionPoint(Map<Point, IntersectionPoint> results, Point point, LineSegment segment1, LineSegment segment2) {
        IntersectionPoint intersectPoint = results.get(point);
        if (intersectPoint == null) {
            intersectPoint = new IntersectionPoint(point);
            results.put(point, intersectPoint);
        }

        intersectPoint.addSegment(segment1);
        intersectPoint.addSegment(segment2);
    }

}
