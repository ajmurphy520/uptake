package uptake.service.geometry;

import uptake.model.Corner;

/**
 * Created by Andrew on 12/12/2015.
 */
public class Point implements Comparable<Point>{

    protected double x;
    protected double y;

    @Override
    public final int compareTo(Point point2) {
        if (HouseValidator.fuzzyMatch(x - point2.x) > 0.0) {
            return 1;
        } else if (HouseValidator.fuzzyMatch(x - point2.x) < 0.0) {
            return -1;
        } else if (HouseValidator.fuzzyMatch(y - point2.y) > 0.0) {
            return 1;
        } else if (HouseValidator.fuzzyMatch(y - point2.y) < 0.0) {
            return -1;
        } else {
            return 0;
        }
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point(Corner corner) {
        this.x = corner.getX();
        this.y = corner.getY();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        if (HouseValidator.fuzzyMatch(x - point.x) != 0.0) return false;
        return HouseValidator.fuzzyMatch(y - point.y) == 0.0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
