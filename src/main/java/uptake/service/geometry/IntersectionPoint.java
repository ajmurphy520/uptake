package uptake.service.geometry;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Andrew on 12/12/2015.
 */
public class IntersectionPoint {

    private Point intersectionPoint;
    private Set<LineSegment> segmentsIntersecting;

    public IntersectionPoint(Point point) {
        this.intersectionPoint = point;
    }

    public void addSegment(LineSegment segment) {
        if (segmentsIntersecting == null) {
            segmentsIntersecting = new HashSet<LineSegment>();
        }
        segmentsIntersecting.add(segment);
    }

    public Point getIntersectionPoint() {
        return intersectionPoint;
    }

    public Set<LineSegment> getSegmentsIntersecting() {
        return segmentsIntersecting;
    }
}
