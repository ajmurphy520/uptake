package uptake.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import uptake.exception.*;
import uptake.model.Floor;
import uptake.model.House;
import uptake.model.Room;
import uptake.service.HouseService;

/**
 * Created by Andrew on 12/9/2015.
 */
@RestController
public class HouseController {

    @Autowired
    HouseService houseService;

    private static final Logger log = LoggerFactory.getLogger(HouseController.class);

    @RequestMapping(value = "/houses", method = RequestMethod.POST)
    public ResponseEntity<?> createHouse(@RequestBody House house, UriComponentsBuilder ucBuilder) {
        try {
            return ResponseEntity.created(ucBuilder.path("/houses/{id}").buildAndExpand(houseService.createHouse(house)).toUri()).build();
        } catch (HouseNotValidException e) {
            log.error("createHouse", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @RequestMapping(value = "/houses/{houseId}", method = RequestMethod.GET)
    public ResponseEntity<?> getHouse(@PathVariable("houseId") long houseId) {
        House house = houseService.getHouse(houseId);
        if (house != null) {
            return ResponseEntity.ok(house);
        }
        return ResponseEntity.notFound().build();
    }

    @RequestMapping(value = "/houses/{houseId}", method = RequestMethod.PUT)
    public ResponseEntity updateHouse(@PathVariable("houseId") long houseId, @RequestBody House house) {
        house.setId(houseId);
        try {
            houseService.updateHouse(house);
            return ResponseEntity.ok().build();
        } catch (HouseNotValidException e) {
            log.error("updateHouse", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (HouseNotFoundException e) {
            log.error("updateHouse", e);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/houses/{houseId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteHouse(@PathVariable("houseId") long houseId) {
        try {
            houseService.deleteHouse(houseId);
            return ResponseEntity.ok().build();
        } catch (HouseNotFoundException e) {
            log.error("deleteHouse", e);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/houses/{houseId}/area", method = RequestMethod.GET)
    public ResponseEntity<?> getHouseArea(@PathVariable("houseId") long houseId) {
        try {
            return ResponseEntity.ok(houseService.getHouseArea(houseId));
        } catch (HouseNotFoundException e) {
            log.error("getHouseArea", e);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/houses/{houseId}/floors", method = RequestMethod.POST)
    public ResponseEntity<?> createFloor(@PathVariable("houseId") long houseId, @RequestBody Floor floor, UriComponentsBuilder ucBuilder) {
        try {
            long floorId = houseService.createFloor(houseId, floor);
            return ResponseEntity.created(ucBuilder.path("/houses/{houseId}/floors/{floorNumber}").buildAndExpand(houseId, floorId).toUri()).build();
        } catch (HouseNotFoundException e) {
            log.error("createFloor", e);
            return ResponseEntity.notFound().build();
        } catch (FloorNotValidException e) {
            log.error("createFloor", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @RequestMapping(value = "/houses/{houseId}/floors/{floorNumber}", method = RequestMethod.GET)
    public ResponseEntity<?> getFloor(@PathVariable("houseId") long houseId, @PathVariable("floorNumber") int floorNumber) {
        try {
            Floor floor = houseService.getFloor(houseId, floorNumber);
            if (floor != null) {
                return ResponseEntity.ok(floor);
            }
            return ResponseEntity.notFound().build();
        } catch (HouseNotFoundException e) {
            log.error("getFloor", e);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/houses/{houseId}/floors/{floorNumber}", method = RequestMethod.PUT)
    public ResponseEntity updateFloor(@PathVariable("houseId") long houseId, @PathVariable("floorNumber") int floorNumber, @RequestBody Floor floor) {
        floor.setFloorNumber(floorNumber);
        try {
            houseService.updateFloor(houseId, floor);
            return ResponseEntity.ok().build();
        } catch (HouseNotFoundException e) {
            log.error("updateFloor", e);
            return ResponseEntity.notFound().build();
        } catch (FloorNotFoundException e) {
            log.error("updateFloor", e);
            return ResponseEntity.notFound().build();
        } catch (FloorNotValidException e) {
            log.error("updateFloor", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @RequestMapping(value = "/houses/{houseId}/floors/{floorNumber}", method = RequestMethod.DELETE)
    public ResponseEntity deleteFloor(@PathVariable("houseId") long houseId, @PathVariable("floorNumber") int floorNumber) {
        try {
            houseService.deleteFloor(houseId, floorNumber);
            return ResponseEntity.ok().build();
        } catch (HouseNotFoundException e) {
            log.error("deleteFloor", e);
            return ResponseEntity.notFound().build();
        } catch (FloorNotFoundException e) {
            log.error("deleteFloor", e);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/houses/{houseId}/floors/{floorNumber}/area", method = RequestMethod.GET)
    public ResponseEntity<?> getFloorArea(@PathVariable("houseId") long houseId, @PathVariable("floorNumber") int floorNumber) {
        try {
            return ResponseEntity.ok(houseService.getFloorArea(houseId, floorNumber));
        } catch (HouseNotFoundException e) {
            log.error("getFloorArea", e);
            return ResponseEntity.notFound().build();
        } catch (FloorNotFoundException e) {
            log.error("getFloorArea", e);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/houses/{houseId}/floors/{floorNumber}/rooms", method = RequestMethod.POST)
    public ResponseEntity<?> createRoom(@PathVariable("houseId") long houseId, @PathVariable("floorNumber") int floorNumber, @RequestBody Room room, UriComponentsBuilder ucBuilder) {
        try {
            long roomId = houseService.createRoom(houseId, floorNumber, room);
            return ResponseEntity.created(ucBuilder.path("/houses/{houseId}/floors/{floorNumber}/rooms/{roomId}").buildAndExpand(houseId, floorNumber, roomId).toUri()).build();
        } catch (HouseNotFoundException e) {
            log.error("createRoom", e);
            return ResponseEntity.notFound().build();
        } catch (FloorNotFoundException e) {
            log.error("createRoom", e);
            return ResponseEntity.notFound().build();
        } catch (RoomNotValidException e) {
            log.error("createRoom", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @RequestMapping(value = "/houses/{houseId}/floors/{floorNumber}/rooms/{roomId}", method = RequestMethod.GET)
    public ResponseEntity<?> getRoom(@PathVariable("houseId") long houseId, @PathVariable("floorNumber") int floorNumber, @PathVariable("roomId") long roomId) {
        try {
            Room room = houseService.getRoom(houseId, floorNumber, roomId);
            if (room != null) {
                return ResponseEntity.ok(room);
            }
            return ResponseEntity.notFound().build();
        } catch (HouseNotFoundException e) {
            log.error("getRoom", e);
            return ResponseEntity.notFound().build();
        } catch (FloorNotFoundException e) {
            log.error("getRoom", e);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/houses/{houseId}/floors/{floorNumber}/rooms/{roomId}", method = RequestMethod.PUT)
    public ResponseEntity updateRoom(@PathVariable("houseId") long houseId, @PathVariable("floorNumber") int floorNumber, @PathVariable("roomId") long roomId, @RequestBody Room room) {
        room.setId(roomId);
        try {
            houseService.updateRoom(houseId, floorNumber, room);
            return ResponseEntity.ok().build();
        } catch (HouseNotFoundException e) {
            log.error("updateRoom", e);
            return ResponseEntity.notFound().build();
        } catch (FloorNotFoundException e) {
            log.error("updateRoom", e);
            return ResponseEntity.notFound().build();
        } catch (RoomNotValidException e) {
            log.error("updateRoom", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (RoomNotFoundException e) {
            log.error("updateRoom", e);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/houses/{houseId}/floors/{floorNumber}/rooms/{roomId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteRoom(@PathVariable("houseId") long houseId, @PathVariable("floorNumber") int floorNumber, @PathVariable("roomId") long roomId) {
        try {
            houseService.deleteRoom(houseId, floorNumber, roomId);
            return ResponseEntity.ok().build();
        } catch (HouseNotFoundException e) {
            log.error("deleteRoom", e);
            return ResponseEntity.notFound().build();
        } catch (FloorNotFoundException e) {
            log.error("deleteRoom", e);
            return ResponseEntity.notFound().build();
        } catch (RoomNotFoundException e) {
            log.error("deleteRoom", e);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/houses/{houseId}/floors/{floorNumber}/rooms/{roomId}/area", method = RequestMethod.GET)
    public ResponseEntity<?> getRoomArea(@PathVariable("houseId") long houseId, @PathVariable("floorNumber") int floorNumber, @PathVariable("roomId") long roomId) {
        try {
            return ResponseEntity.ok(houseService.getRoomArea(houseId, floorNumber, roomId));
        } catch (HouseNotFoundException e) {
            log.error("getRoomArea", e);
            return ResponseEntity.notFound().build();
        } catch (FloorNotFoundException e) {
            log.error("getRoomArea", e);
            return ResponseEntity.notFound().build();
        } catch (RoomNotFoundException e) {
            log.error("getRoomArea", e);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/houses", method = RequestMethod.DELETE)
    public ResponseEntity deleteAllHouses() {
        houseService.cleanDatabase();
        return ResponseEntity.ok().build();
    }

}
