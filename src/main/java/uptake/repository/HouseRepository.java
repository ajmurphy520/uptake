package uptake.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import uptake.model.House;

/**
 * Created by Andrew on 12/11/2015.
 */
public interface HouseRepository extends ElasticsearchRepository<House, Long> {

    House findOneByAddress(String address);

}
