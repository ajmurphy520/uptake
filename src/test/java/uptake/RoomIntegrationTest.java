package uptake;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import uptake.exception.*;
import uptake.model.Corner;
import uptake.model.Floor;
import uptake.model.House;
import uptake.model.Room;

import java.net.URI;
import java.util.LinkedHashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Andrew on 12/13/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class RoomIntegrationTest {

    private final String BASE_URL = "http://localhost:8080";

    static LinkedHashSet<Corner> corners;

    @BeforeClass
    public static void createCorners() {
        corners = new LinkedHashSet<Corner>();
        corners.add(new Corner(0, 0));
        corners.add(new Corner(0, 10));
        corners.add(new Corner(10, 10));
        corners.add(new Corner(10, 0));
    }

    @Before
    public void createFloor() throws HouseNotValidException {
        RestTemplate template = new RestTemplate();
        template.delete(BASE_URL + "/houses");
        House testHouse = new House();
        Floor testFloor = new Floor();
        testFloor.setFloorNumber(1);
        testHouse.getFloors().put(testFloor.getFloorNumber(), testFloor);
        URI location = template.postForLocation(BASE_URL + "/houses/", testHouse, House.class);
        assertEquals(location.getPath(), "/houses/1");
    }

    @Test
    public void testCreateRoom() throws HouseNotFoundException, FloorNotFoundException, RoomNotValidException {
        RestTemplate template = new RestTemplate();
        Room testRoom = new Room();
        testRoom.setCorners(corners);
        URI location = template.postForLocation(BASE_URL + "/houses/1/floors/1/rooms", testRoom, Room.class);
        assertEquals(location.getPath(), "/houses/1/floors/1/rooms/2");
    }

    @Test
    public void testReadRoom() throws HouseNotFoundException, FloorNotFoundException, RoomNotValidException {
        RestTemplate template = new RestTemplate();
        Room testRoom = new Room();
        testRoom.setCorners(corners);
        URI location = template.postForLocation(BASE_URL + "/houses/1/floors/1/rooms", testRoom, Room.class);
        testRoom.setId(2L);
        Room testRoom2 = template.getForEntity(location, Room.class).getBody();
        assertEquals(testRoom, testRoom2);
    }

    @Test
    public void testUpdateRoom() throws HouseNotFoundException, FloorNotFoundException, RoomNotValidException, RoomNotFoundException {
        RestTemplate template = new RestTemplate();
        Room testRoom = new Room();
        testRoom.setCorners(corners);
        URI location = template.postForLocation(BASE_URL + "/houses/1/floors/1/rooms", testRoom, Room.class);
        testRoom.setId(2L);
        Room testRoom2 = template.getForEntity(location, Room.class).getBody();
        assertEquals(testRoom, testRoom2);
        testRoom.setName("Bedroom");
        testRoom.setId(2L);
        Room testRoom3 = template.getForEntity(location, Room.class).getBody();
        assertEquals(testRoom, testRoom3);
    }

    @Test
    public void testDeleteRoom() throws HouseNotFoundException, FloorNotFoundException, RoomNotValidException, RoomNotFoundException {
        RestTemplate template = new RestTemplate();
        Room testRoom = new Room();
        testRoom.setCorners(corners);
        URI location = template.postForLocation(BASE_URL + "/houses/1/floors/1/rooms", testRoom, Room.class);
        template.delete(location);
        try {
            template.getForEntity(location, Room.class).getBody();
        } catch (HttpClientErrorException e) {
            assertTrue(e.getStatusCode().equals(HttpStatus.NOT_FOUND));
        }
    }

    @Test
    public void testSelfIntersectingRoom() throws HouseNotFoundException, FloorNotFoundException {
        RestTemplate template = new RestTemplate();
        LinkedHashSet<Corner> corners2 = new LinkedHashSet<Corner>(corners);
        corners2.add(new Corner(5, 10));
        Room testRoom = new Room();
        testRoom.setCorners(corners2);
        try {
            URI location = template.postForLocation(BASE_URL + "/houses/1/floors/1/rooms", testRoom, Room.class);
            assertTrue(false);
        } catch (HttpClientErrorException e) {
            assertTrue(e.getStatusCode().equals(HttpStatus.BAD_REQUEST));
        }
    }

    @Test
    public void testTooFewCorners() throws HouseNotFoundException, FloorNotFoundException {
        RestTemplate template = new RestTemplate();
        LinkedHashSet<Corner> corners2 = new LinkedHashSet<Corner>();
        corners2.add(new Corner(5, 10));
        Room testRoom = new Room();
        testRoom.setCorners(corners2);
        try {
            URI location = template.postForLocation(BASE_URL + "/houses/1/floors/1/rooms", testRoom, Room.class);
            assertTrue(false);
        } catch (HttpClientErrorException e) {
            assertTrue(e.getStatusCode().equals(HttpStatus.BAD_REQUEST));
        }
    }

    @Test
    public void testSimpleArea() {
        RestTemplate template = new RestTemplate();
        double testValue = 125;
        LinkedHashSet<Corner> corners2 = new LinkedHashSet<Corner>();
        corners2.add(new Corner(0, 5));
        corners2.add(new Corner(0, 10));
        corners2.add(new Corner(10, 10));
        corners2.add(new Corner(10, 5));
        corners2.add(new Corner(20, 5));
        corners2.add(new Corner(20, 0));
        corners2.add(new Corner(5, 0));
        corners2.add(new Corner(5, 5));
        Room testRoom = new Room();
        testRoom.setCorners(corners2);
        assertTrue(testRoom.getArea() == testValue);
        URI location = template.postForLocation(BASE_URL + "/houses/1/floors/1/rooms", testRoom, Room.class);
        double resultArea = template.getForEntity(location.toString() + "/area", Double.TYPE).getBody();
        assertTrue(String.valueOf(resultArea), resultArea == testValue);
    }

    @Test
    public void testComplexArea() {
        RestTemplate template = new RestTemplate();
        double testValue = 82.6067;
        LinkedHashSet<Corner> corners2 = new LinkedHashSet<Corner>();
        corners2.add(new Corner(2.218, 9.016));
        corners2.add(new Corner(7.168, 8.796));
        corners2.add(new Corner(13.878, 5.188));
        corners2.add(new Corner(18.322, 6.86));
        corners2.add(new Corner(18.256, 1.404));
        corners2.add(new Corner(11.348, 4.704));
        corners2.add(new Corner(9.632, -0.18));
        corners2.add(new Corner(2.394, 2.636));
        Room testRoom = new Room();
        testRoom.setCorners(corners2);
        assertTrue(testRoom.getArea() == testValue);
        URI location = template.postForLocation(BASE_URL + "/houses/1/floors/1/rooms", testRoom, Room.class);
        double resultArea = template.getForEntity(location.toString() + "/area", Double.TYPE).getBody();
        assertTrue(String.valueOf(resultArea), resultArea == testValue);
    }

    @Test
    public void testComplexAreaReverse() {
        RestTemplate template = new RestTemplate();
        double testValue = 82.6067;
        LinkedHashSet<Corner> corners2 = new LinkedHashSet<Corner>();
        corners2.add(new Corner(2.394, 2.636));
        corners2.add(new Corner(9.632, -0.18));
        corners2.add(new Corner(11.348, 4.704));
        corners2.add(new Corner(18.256, 1.404));
        corners2.add(new Corner(18.322, 6.86));
        corners2.add(new Corner(13.878, 5.188));
        corners2.add(new Corner(7.168, 8.796));
        corners2.add(new Corner(2.218, 9.016));
        Room testRoom = new Room();
        testRoom.setCorners(corners2);
        assertTrue(testRoom.getArea() == testValue);
        URI location = template.postForLocation(BASE_URL + "/houses/1/floors/1/rooms", testRoom, Room.class);
        double resultArea = template.getForEntity(location.toString() + "/area", Double.TYPE).getBody();
        assertTrue(String.valueOf(resultArea), resultArea == testValue);
    }
}
