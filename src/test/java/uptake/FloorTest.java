package uptake;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uptake.exception.FloorNotFoundException;
import uptake.exception.FloorNotValidException;
import uptake.exception.HouseNotFoundException;
import uptake.exception.HouseNotValidException;
import uptake.model.Corner;
import uptake.model.Floor;
import uptake.model.House;
import uptake.model.Room;
import uptake.service.HouseService;

import java.util.LinkedHashSet;

import static org.junit.Assert.*;

/**
 * Created by Andrew on 12/12/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class FloorTest {

    @Autowired
    HouseService service;

    @Before
    public void setupHouseForTest() throws HouseNotValidException {
        service.cleanDatabase();
        House testHouse = new House();
        long response = service.createHouse(testHouse);
        assertTrue(response == 1);
    }

    @Test
    public void testCreateFloor() throws HouseNotFoundException, FloorNotValidException {
        Floor testFloor = new Floor();
        testFloor.setFloorNumber(1);
        service.createFloor(1, testFloor);
    }

    @Test
    public void testReadHouse() throws HouseNotFoundException, FloorNotValidException {
        Floor testFloor = new Floor();
        testFloor.setFloorNumber(1);
        service.createFloor(1, testFloor);
        Floor testFloor2 = service.getFloor(1, testFloor.getFloorNumber());
        assertEquals(testFloor, testFloor2);
    }

    @Test
    public void testUpdateFloor() throws HouseNotFoundException, FloorNotValidException, FloorNotFoundException {
        Floor testFloor = new Floor();
        testFloor.setFloorNumber(1);
        service.createFloor(1, testFloor);
        Floor testFloor2 = service.getFloor(1, testFloor.getFloorNumber());
        assertEquals(testFloor, testFloor2);
        Room testRoom = new Room();
        LinkedHashSet<Corner> corners = new LinkedHashSet<Corner>();
        corners.add(new Corner(0, 0));
        corners.add(new Corner(0, 10));
        corners.add(new Corner(10, 10));
        corners.add(new Corner(10, 0));
        testRoom.setCorners(corners);
        testRoom.setId(1L);
        testFloor.addRoom(testRoom);
        service.updateFloor(1,testFloor);
        Floor testFloor3 = service.getFloor(1, testFloor.getFloorNumber());
        assertNotEquals(testFloor2, testFloor3);
    }

    @Test
    public void testDeleteFloor() throws HouseNotFoundException, FloorNotValidException, FloorNotFoundException {
        Floor testFloor = new Floor();
        testFloor.setFloorNumber(1);
        service.createFloor(1, testFloor);
        assertTrue(service.existsFloor(1, testFloor.getFloorNumber()));
        service.deleteFloor(1, testFloor.getFloorNumber());
        assertFalse(service.existsFloor(1, testFloor.getFloorNumber()));
    }

    @Test
    public void testUniqueFloorNumber() throws HouseNotFoundException, FloorNotValidException {
        Floor testFloor = new Floor();
        testFloor.setFloorNumber(1);
        service.createFloor(1, testFloor);
        try {
            service.createFloor(1, testFloor);
            assertTrue(false);
        } catch (FloorNotValidException e) {
            //do nothing test passes
        }
    }

    @Test
    public void testFloorArea() throws HouseNotFoundException, FloorNotValidException {
        Floor testFloor = new Floor();
        testFloor.setFloorNumber(1);

        LinkedHashSet<Corner> corners = new LinkedHashSet<Corner>();
        corners.add(new Corner(2.394, 2.636));
        corners.add(new Corner(9.632, -0.18));
        corners.add(new Corner(11.348, 4.704));
        corners.add(new Corner(18.256, 1.404));
        corners.add(new Corner(18.322, 6.86));
        corners.add(new Corner(13.878, 5.188));
        corners.add(new Corner(7.168, 8.796));
        corners.add(new Corner(2.218, 9.016));
        Room testRoom1 = new Room();
        testRoom1.setCorners(corners);
        testRoom1.setId(1L);
        corners = new LinkedHashSet<Corner>();

        corners.add(new Corner(102.394, 2.636));
        corners.add(new Corner(109.632, -0.18));
        corners.add(new Corner(111.348, 4.704));
        corners.add(new Corner(118.256, 1.404));
        corners.add(new Corner(118.322, 6.86));
        corners.add(new Corner(113.878, 5.188));
        corners.add(new Corner(107.168, 8.796));
        corners.add(new Corner(102.218, 9.016));
        Room testRoom2 = new Room();
        testRoom2.setCorners(corners);
        testRoom2.setId(2L);

        corners = new LinkedHashSet<Corner>();
        corners.add(new Corner(202.394, 2.636));
        corners.add(new Corner(209.632, -0.18));
        corners.add(new Corner(211.348, 4.704));
        corners.add(new Corner(218.256, 1.404));
        corners.add(new Corner(218.322, 6.86));
        corners.add(new Corner(213.878, 5.188));
        corners.add(new Corner(207.168, 8.796));
        corners.add(new Corner(202.218, 9.016));
        Room testRoom3 = new Room();
        testRoom3.setCorners(corners);
        testRoom3.setId(3L);

        corners = new LinkedHashSet<Corner>();
        corners.add(new Corner(302.394, 2.636));
        corners.add(new Corner(309.632, -0.18));
        corners.add(new Corner(311.348, 4.704));
        corners.add(new Corner(318.256, 1.404));
        corners.add(new Corner(318.322, 6.86));
        corners.add(new Corner(313.878, 5.188));
        corners.add(new Corner(307.168, 8.796));
        corners.add(new Corner(302.218, 9.016));
        Room testRoom4 = new Room();
        testRoom4.setCorners(corners);
        testRoom4.setId(4L);

        testFloor.addRoom(testRoom1);
        testFloor.addRoom(testRoom2);
        testFloor.addRoom(testRoom3);
        testFloor.addRoom(testRoom4);
        service.createFloor(1, testFloor);

        assertTrue(String.valueOf(testFloor.getArea()), testFloor.getArea() == (4 * 82.6067));
    }
}
