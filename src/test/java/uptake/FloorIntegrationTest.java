package uptake;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import uptake.exception.FloorNotFoundException;
import uptake.exception.FloorNotValidException;
import uptake.exception.HouseNotFoundException;
import uptake.exception.HouseNotValidException;
import uptake.model.Corner;
import uptake.model.Floor;
import uptake.model.House;
import uptake.model.Room;

import java.net.URI;
import java.util.LinkedHashSet;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

/**
 * Created by Andrew on 12/13/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class FloorIntegrationTest {

    private final String BASE_URL = "http://localhost:8080";

    @Before
    public void setupHouseForTest() throws HouseNotValidException {
        RestTemplate template = new RestTemplate();
        template.delete(BASE_URL + "/houses");
        House testHouse = new House();
        URI location = template.postForLocation(BASE_URL + "/houses", testHouse, House.class);
        assertEquals(location.getPath(), "/houses/1");
    }

    @Test
    public void testCreateFloor() throws HouseNotFoundException, FloorNotValidException {
        Floor testFloor = new Floor();
        testFloor.setFloorNumber(1);
        RestTemplate template = new RestTemplate();
        URI location = template.postForLocation(BASE_URL + "/houses/1/floors", testFloor, Floor.class);
        assertEquals(location.getPath(), "/houses/1/floors/1");
    }

    @Test
    public void testReadHouse() throws HouseNotFoundException, FloorNotValidException {
        Floor testFloor = new Floor();
        testFloor.setFloorNumber(1);
        RestTemplate template = new RestTemplate();
        URI location = template.postForLocation(BASE_URL + "/houses/1/floors", testFloor, Floor.class);
        Floor testFloor2 = template.getForEntity(location, Floor.class).getBody();
        assertEquals(testFloor, testFloor2);
    }

    @Test
    public void testUpdateFloor() throws HouseNotFoundException, FloorNotValidException, FloorNotFoundException {
        Floor testFloor = new Floor();
        testFloor.setFloorNumber(1);
        RestTemplate template = new RestTemplate();
        URI location = template.postForLocation(BASE_URL + "/houses/1/floors", testFloor, Floor.class);
        Floor testFloor2 = template.getForEntity(location, Floor.class).getBody();
        assertEquals(testFloor, testFloor2);
        Room testRoom = new Room();
        LinkedHashSet<Corner> corners = new LinkedHashSet<Corner>();
        corners.add(new Corner(0, 0));
        corners.add(new Corner(0, 10));
        corners.add(new Corner(10, 10));
        corners.add(new Corner(10, 0));
        testRoom.setCorners(corners);
        testRoom.setId(1L);
        testFloor.addRoom(testRoom);
        template.put(location, testFloor);
        Floor testFloor3 = template.getForEntity(location, Floor.class).getBody();
        assertEquals(testFloor, testFloor3);
    }

    @Test
    public void testDeleteFloor() throws HouseNotFoundException, FloorNotValidException, FloorNotFoundException {
        Floor testFloor = new Floor();
        testFloor.setFloorNumber(1);
        RestTemplate template = new RestTemplate();
        URI location = template.postForLocation(BASE_URL + "/houses/1/floors", testFloor, Floor.class);
        template.delete(location);
        try {
            template.getForEntity(location, Floor.class).getBody();
        } catch (HttpClientErrorException e) {
            assertTrue(e.getStatusCode().equals(HttpStatus.NOT_FOUND));
        }
    }

    @Test
    public void testUniqueFloorNumber() throws HouseNotFoundException, FloorNotValidException {
        Floor testFloor = new Floor();
        testFloor.setFloorNumber(1);
        RestTemplate template = new RestTemplate();
        URI location = template.postForLocation(BASE_URL + "/houses/1/floors", testFloor, Floor.class);
        try {
            template.postForEntity(BASE_URL + "/houses/1/floors", testFloor, Floor.class);
        } catch (HttpClientErrorException e) {
            assertTrue(e.getStatusCode() == HttpStatus.BAD_REQUEST);
        }
    }

    @Test
    public void testFloorArea() throws HouseNotFoundException, FloorNotValidException {
        Floor testFloor = new Floor();
        testFloor.setFloorNumber(1);

        LinkedHashSet<Corner> corners = new LinkedHashSet<Corner>();
        corners.add(new Corner(2.394, 2.636));
        corners.add(new Corner(9.632, -0.18));
        corners.add(new Corner(11.348, 4.704));
        corners.add(new Corner(18.256, 1.404));
        corners.add(new Corner(18.322, 6.86));
        corners.add(new Corner(13.878, 5.188));
        corners.add(new Corner(7.168, 8.796));
        corners.add(new Corner(2.218, 9.016));
        Room testRoom1 = new Room();
        testRoom1.setCorners(corners);
        testRoom1.setId(1L);
        corners = new LinkedHashSet<Corner>();

        corners.add(new Corner(102.394, 2.636));
        corners.add(new Corner(109.632, -0.18));
        corners.add(new Corner(111.348, 4.704));
        corners.add(new Corner(118.256, 1.404));
        corners.add(new Corner(118.322, 6.86));
        corners.add(new Corner(113.878, 5.188));
        corners.add(new Corner(107.168, 8.796));
        corners.add(new Corner(102.218, 9.016));
        Room testRoom2 = new Room();
        testRoom2.setCorners(corners);
        testRoom2.setId(2L);

        corners = new LinkedHashSet<Corner>();
        corners.add(new Corner(202.394, 2.636));
        corners.add(new Corner(209.632, -0.18));
        corners.add(new Corner(211.348, 4.704));
        corners.add(new Corner(218.256, 1.404));
        corners.add(new Corner(218.322, 6.86));
        corners.add(new Corner(213.878, 5.188));
        corners.add(new Corner(207.168, 8.796));
        corners.add(new Corner(202.218, 9.016));
        Room testRoom3 = new Room();
        testRoom3.setCorners(corners);
        testRoom3.setId(3L);

        corners = new LinkedHashSet<Corner>();
        corners.add(new Corner(302.394, 2.636));
        corners.add(new Corner(309.632, -0.18));
        corners.add(new Corner(311.348, 4.704));
        corners.add(new Corner(318.256, 1.404));
        corners.add(new Corner(318.322, 6.86));
        corners.add(new Corner(313.878, 5.188));
        corners.add(new Corner(307.168, 8.796));
        corners.add(new Corner(302.218, 9.016));
        Room testRoom4 = new Room();
        testRoom4.setCorners(corners);
        testRoom4.setId(4L);

        testFloor.addRoom(testRoom1);
        testFloor.addRoom(testRoom2);
        testFloor.addRoom(testRoom3);
        testFloor.addRoom(testRoom4);

        RestTemplate template = new RestTemplate();
        URI location = template.postForLocation(BASE_URL + "/houses/1/floors", testFloor, Floor.class);

        double testValue = (double) Math.round(4 * 82.6067 * 100000d) / 100000d;
        double resultArea = template.getForEntity(location.toString() + "/area", Double.TYPE).getBody();
        assertTrue(String.valueOf(resultArea), resultArea == testValue);
    }
}
