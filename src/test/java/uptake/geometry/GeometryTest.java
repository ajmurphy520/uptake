package uptake.geometry;

import org.junit.Test;
import uptake.service.geometry.LineSegment;
import uptake.service.geometry.Point;

import static org.junit.Assert.*;

/**
 * Created by Andrew on 12/12/2015.
 */
public class GeometryTest {

    @Test
    public void testPointRelativeToLine() {
        LineSegment segement = new LineSegment(1, 1, 1, 2);
        assertTrue(segement.locationOfPointRelativeToSegment(new Point(0, 3)) > 0.0);
        assertTrue(segement.locationOfPointRelativeToSegment(new Point(2, 3)) < 0.0);
        assertTrue(segement.locationOfPointRelativeToSegment(new Point(1, 1.5)) == 0.0);
        assertTrue(segement.locationOfPointRelativeToSegment(new Point(1, 1)) == 0.0);
    }

    @Test
    public void testNonEndPointIntersection() {
        LineSegment segment1 = new LineSegment(1, 1, 1, 3);
        LineSegment segment2 = new LineSegment(0, 2, 2, 2);
        Point intersetion = LineSegment.intersectExcludeEndPoints(segment1, segment2);
        assertNotNull(intersetion);
        assertTrue(intersetion.getX() == 1);
        assertTrue(intersetion.getY() == 2);
    }

    @Test
    public void testNonIntersecting() {
        LineSegment segment1 = new LineSegment(1, 1, 1, 3);
        LineSegment segment2 = new LineSegment(0, 4, 2, 4);
        Point intersetion = LineSegment.intersectExcludeEndPoints(segment1, segment2);
        assertNull(intersetion);
    }

    @Test
    public void testParallel() {
        LineSegment segment1 = new LineSegment(0, 3, 2, 3);
        LineSegment segment2 = new LineSegment(0, 4, 2, 4);
        Point intersetion = LineSegment.intersectExcludeEndPoints(segment1, segment2);
        assertNull(intersetion);
        segment1 = new LineSegment(1, 1, 1, 3);
        segment2 = new LineSegment(2, 1, 2, 3);
        intersetion = LineSegment.intersectExcludeEndPoints(segment1, segment2);
        assertNull(intersetion);
    }

    @Test
    public void testColinear() {
        LineSegment segment1 = new LineSegment(0, 3, 2, 3);
        LineSegment segment2 = new LineSegment(0, 3, 2, 3);
        Point intersetion = LineSegment.intersectExcludeEndPoints(segment1, segment2);
        assertNull(intersetion);
        segment1 = new LineSegment(1, 1, 1, 3);
        segment2 = new LineSegment(1, 1, 1, 3);
        intersetion = LineSegment.intersectExcludeEndPoints(segment1, segment2);
        assertNull(intersetion);
    }

    @Test
    public void testSingleEndpointIntersect() {
        LineSegment segment1 = new LineSegment(1, 1, 1, 2);
        LineSegment segment2 = new LineSegment(0, 2, 2, 2);
        Point intersetion = LineSegment.intersectExcludeEndPoints(segment1, segment2);
        assertNull(intersetion);
    }
}
