package uptake;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uptake.exception.*;
import uptake.model.Corner;
import uptake.model.Floor;
import uptake.model.House;
import uptake.model.Room;
import uptake.service.HouseService;

import java.util.LinkedHashSet;

import static org.junit.Assert.*;

/**
 * Created by Andrew on 12/12/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class RoomTest {

    @Autowired
    HouseService service;

    static LinkedHashSet<Corner> corners;

    @BeforeClass
    public static void createCorners() {
        corners = new LinkedHashSet<Corner>();
        corners.add(new Corner(0, 0));
        corners.add(new Corner(0, 10));
        corners.add(new Corner(10, 10));
        corners.add(new Corner(10, 0));
    }

    @Before
    public void createFloor() throws HouseNotValidException {
        service.cleanDatabase();
        House testHouse = new House();
        Floor testFloor = new Floor();
        testFloor.setFloorNumber(1);
        testHouse.getFloors().put(testFloor.getFloorNumber(), testFloor);
        long response = service.createHouse(testHouse);
        assertTrue(response == 1);
    }

    @Test
    public void testCreateRoom() throws HouseNotFoundException, FloorNotFoundException, RoomNotValidException {
        Room testRoom = new Room();
        testRoom.setCorners(corners);
        service.createRoom(1, 1, testRoom);
    }

    @Test
    public void testReadRoom() throws HouseNotFoundException, FloorNotFoundException, RoomNotValidException {
        Room testRoom = new Room();
        testRoom.setCorners(corners);
        long response = service.createRoom(1, 1, testRoom);
        testRoom.setId(response);
        Room testRoom2 = service.getRoom(1, 1, response);
        assertEquals(testRoom, testRoom2);
    }

    @Test
    public void testUpdateRoom() throws HouseNotFoundException, FloorNotFoundException, RoomNotValidException, RoomNotFoundException {
        Room testRoom = new Room();
        testRoom.setCorners(corners);
        long response = service.createRoom(1, 1, testRoom);
        testRoom.setId(response);
        Room testRoom2 = service.getRoom(1, 1, response);
        assertEquals(testRoom, testRoom2);
        testRoom.setName("Bedroom");
        service.updateRoom(1, 1, testRoom);
        Room testRoom3 = service.getRoom(1, 1, testRoom.getId());
        assertFalse(testRoom3.getName().equals(testRoom2.getName()));
    }

    @Test
    public void testDeleteRoom() throws HouseNotFoundException, FloorNotFoundException, RoomNotValidException, RoomNotFoundException {
        Room testRoom = new Room();
        testRoom.setCorners(corners);
        long response = service.createRoom(1, 1, testRoom);
        testRoom.setId(response);
        assertTrue(service.existsRoom(1, 1, testRoom.getId()));
        service.deleteRoom(1, 1, testRoom.getId());
        assertFalse(service.existsRoom(1, 1, testRoom.getId()));
    }

    @Test
    public void testSelfIntersectingRoom() throws HouseNotFoundException, FloorNotFoundException {
        LinkedHashSet<Corner> corners2 = new LinkedHashSet<Corner>(corners);
        corners2.add(new Corner(5, 10));
        Room testRoom = new Room();
        testRoom.setCorners(corners2);
        try {
            service.createRoom(1, 1, testRoom);
            assertTrue(false);
        } catch (RoomNotValidException e) {
            //do nothing test passes
        }
    }

    @Test
    public void testTooFewCorners() throws HouseNotFoundException, FloorNotFoundException {
        LinkedHashSet<Corner> corners2 = new LinkedHashSet<Corner>();
        corners2.add(new Corner(5, 10));
        Room testRoom = new Room();
        testRoom.setCorners(corners2);
        try {
            service.createRoom(1, 1, testRoom);
            assertTrue(false);
        } catch (RoomNotValidException e) {
            //do nothing test passes
        }
    }

    @Test
    public void testSimpleArea() {
        LinkedHashSet<Corner> corners2 = new LinkedHashSet<Corner>();
        corners2.add(new Corner(0, 5));
        corners2.add(new Corner(0, 10));
        corners2.add(new Corner(10, 10));
        corners2.add(new Corner(10, 5));
        corners2.add(new Corner(20, 5));
        corners2.add(new Corner(20, 0));
        corners2.add(new Corner(5, 0));
        corners2.add(new Corner(5, 5));
        Room testRoom = new Room();
        testRoom.setCorners(corners2);
        assertTrue(testRoom.getArea() == 125);
    }

    @Test
    public void testComplexArea() {
        LinkedHashSet<Corner> corners2 = new LinkedHashSet<Corner>();
        corners2.add(new Corner(2.218, 9.016));
        corners2.add(new Corner(7.168, 8.796));
        corners2.add(new Corner(13.878, 5.188));
        corners2.add(new Corner(18.322, 6.86));
        corners2.add(new Corner(18.256, 1.404));
        corners2.add(new Corner(11.348, 4.704));
        corners2.add(new Corner(9.632, -0.18));
        corners2.add(new Corner(2.394, 2.636));
        Room testRoom = new Room();
        testRoom.setCorners(corners2);
        assertTrue(testRoom.getArea() == 82.6067);
    }

    @Test
    public void testComplexAreaReverse() {
        LinkedHashSet<Corner> corners2 = new LinkedHashSet<Corner>();
        corners2.add(new Corner(2.394, 2.636));
        corners2.add(new Corner(9.632, -0.18));
        corners2.add(new Corner(11.348, 4.704));
        corners2.add(new Corner(18.256, 1.404));
        corners2.add(new Corner(18.322, 6.86));
        corners2.add(new Corner(13.878, 5.188));
        corners2.add(new Corner(7.168, 8.796));
        corners2.add(new Corner(2.218, 9.016));
        Room testRoom = new Room();
        testRoom.setCorners(corners2);
        assertTrue(testRoom.getArea() == 82.6067);
    }
}
