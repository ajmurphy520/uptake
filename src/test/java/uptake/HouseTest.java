package uptake;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uptake.exception.HouseNotFoundException;
import uptake.exception.HouseNotValidException;
import uptake.model.Corner;
import uptake.model.Floor;
import uptake.model.House;
import uptake.model.Room;
import uptake.service.HouseService;

import java.util.LinkedHashSet;

import static org.junit.Assert.*;

/**
 * Created by Andrew on 12/9/2015.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class HouseTest {

    @Autowired
    HouseService service;

    @Before
    public void cleanDatabase() {
        service.cleanDatabase();
    }

    @Test
    public void testCreateHouse() throws HouseNotValidException {
        House testHouse = new House();
        testHouse.setAddress("6530 Double Eagle Drive");
        service.createHouse(testHouse);
    }

    @Test
    public void testReadHouse() throws HouseNotValidException {
        House testHouse = new House();
        testHouse.setAddress("6530 Double Eagle Drive");
        long response = service.createHouse(testHouse);
        testHouse.setId(response);
        House testHouse2 = service.getHouse(response);
        assertEquals(testHouse, testHouse2);
    }

    @Test
    public void testUpdateHouse() throws HouseNotValidException, HouseNotFoundException {
        House testHouse = new House();
        testHouse.setAddress("6530 Double Eagle Drive");
        long response = service.createHouse(testHouse);
        testHouse.setId(response);
        testHouse.setAddress("6530 Double Eagle Drive #325");
        service.updateHouse(testHouse);
        assertEquals(testHouse, service.getHouse(testHouse.getId()));
    }

    @Test
    public void testDeleteHouse() throws HouseNotValidException, HouseNotFoundException {
        House testHouse = new House();
        testHouse.setAddress("6530 Double Eagle Drive");
        long response = service.createHouse(testHouse);
        assertTrue(service.existsHouse(response));
        service.deleteHouse(response);
        assertFalse(service.existsHouse(response));
    }

    @Test
    public void testUniqueAddress() throws HouseNotValidException {
        House testHouse = new House();
        testHouse.setAddress("6530 Double Eagle Drive");
        service.createHouse(testHouse);
        try {
            service.createHouse(testHouse);
            assertTrue(false);
        } catch (HouseNotValidException e) {
            //do nothing test passes
        }
    }

    @Test
    public void testHouseArea() throws HouseNotValidException {
        House testHouse = new House();
        Floor testFloor1 = new Floor();
        testFloor1.setFloorNumber(1);
        Floor testFloor2 = new Floor();
        testFloor2.setFloorNumber(2);
        Floor testFloor3 = new Floor();
        testFloor3.setFloorNumber(3);

        LinkedHashSet<Corner> corners = new LinkedHashSet<Corner>();
        corners.add(new Corner(2.394, 2.636));
        corners.add(new Corner(9.632, -0.18));
        corners.add(new Corner(11.348, 4.704));
        corners.add(new Corner(18.256, 1.404));
        corners.add(new Corner(18.322, 6.86));
        corners.add(new Corner(13.878, 5.188));
        corners.add(new Corner(7.168, 8.796));
        corners.add(new Corner(2.218, 9.016));
        Room testRoom1 = new Room();
        testRoom1.setCorners(corners);
        testRoom1.setId(1L);
        corners = new LinkedHashSet<Corner>();

        corners.add(new Corner(102.394, 2.636));
        corners.add(new Corner(109.632, -0.18));
        corners.add(new Corner(111.348, 4.704));
        corners.add(new Corner(118.256, 1.404));
        corners.add(new Corner(118.322, 6.86));
        corners.add(new Corner(113.878, 5.188));
        corners.add(new Corner(107.168, 8.796));
        corners.add(new Corner(102.218, 9.016));
        Room testRoom2 = new Room();
        testRoom2.setCorners(corners);
        testRoom2.setId(2L);

        corners = new LinkedHashSet<Corner>();
        corners.add(new Corner(202.394, 2.636));
        corners.add(new Corner(209.632, -0.18));
        corners.add(new Corner(211.348, 4.704));
        corners.add(new Corner(218.256, 1.404));
        corners.add(new Corner(218.322, 6.86));
        corners.add(new Corner(213.878, 5.188));
        corners.add(new Corner(207.168, 8.796));
        corners.add(new Corner(202.218, 9.016));
        Room testRoom3 = new Room();
        testRoom3.setCorners(corners);
        testRoom3.setId(3L);

        corners = new LinkedHashSet<Corner>();
        corners.add(new Corner(302.394, 2.636));
        corners.add(new Corner(309.632, -0.18));
        corners.add(new Corner(311.348, 4.704));
        corners.add(new Corner(318.256, 1.404));
        corners.add(new Corner(318.322, 6.86));
        corners.add(new Corner(313.878, 5.188));
        corners.add(new Corner(307.168, 8.796));
        corners.add(new Corner(302.218, 9.016));
        Room testRoom4 = new Room();
        testRoom4.setCorners(corners);
        testRoom4.setId(4L);

        testFloor1.addRoom(testRoom1);
        testFloor1.addRoom(testRoom2);
        testFloor1.addRoom(testRoom3);
        testFloor1.addRoom(testRoom4);

        testFloor2.addRoom(testRoom1);
        testFloor2.addRoom(testRoom2);
        testFloor2.addRoom(testRoom3);
        testFloor2.addRoom(testRoom4);

        testFloor3.addRoom(testRoom1);
        testFloor3.addRoom(testRoom2);
        testFloor3.addRoom(testRoom3);
        testFloor3.addRoom(testRoom4);

        testHouse.addFloor(testFloor1);
        testHouse.addFloor(testFloor2);
        testHouse.addFloor(testFloor3);
        service.createHouse(testHouse);

        double testValue = (double) Math.round(12 * 82.6067 * 100000d) / 100000d;
        assertTrue(String.valueOf(testHouse.getArea()), testHouse.getArea() == testValue);
    }
}
